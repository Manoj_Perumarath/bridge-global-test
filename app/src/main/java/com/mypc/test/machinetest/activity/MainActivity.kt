package com.mypc.test.machinetest.activity

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import com.mypc.test.machinetest.R
import com.mypc.test.machinetest.fragment.AddUserFragment
import com.mypc.test.machinetest.fragment.ListFragment
import com.mypc.test.machinetest.model.User
import com.mypc.test.machinetest.viewmodel.ListViewModel

class MainActivity : AppCompatActivity() {

    lateinit var listViewModel: ListViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        listViewModel = ViewModelProviders.of(this).get(ListViewModel::class.java)
        if (savedInstanceState == null)
            supportFragmentManager.beginTransaction().add(R.id.fragment_container, ListFragment(), ListFragment.TAG).commit()
    }

    fun openAddUserFragment(arguments: Bundle) {

        Handler().postDelayed(Runnable {
            supportFragmentManager.beginTransaction().replace(R.id.fragment_container, AddUserFragment.newInstance(bundle = arguments)).addToBackStack(null).commit()
        }, 2000)
        //"Delay was added because when the dialog fragment is dismissed that action triggers to close the replacing fragment")
    }

    fun updateUser(user: User) {
        listViewModel.userList.add(user)
    }

}
