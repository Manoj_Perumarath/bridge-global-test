package com.mypc.test.machinetest.adapter

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mypc.test.machinetest.R
import com.mypc.test.machinetest.databinding.ItemUserBinding
import com.mypc.test.machinetest.interfaces.UserClickListener
import com.mypc.test.machinetest.model.User

class UserAdapter(private val userList: ArrayList<User>, private val clickListener: UserClickListener) : RecyclerView.Adapter<UserAdapter.CustomViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        return CustomViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_user, parent, false))
    }

    override fun getItemCount(): Int {
        return userList.size
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        holder.binding.user = userList[position]
        holder.binding.executePendingBindings()
        holder.itemView.setOnClickListener { clickListener.onItemClickListener(position) }
    }

    inner class CustomViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
        var binding: ItemUserBinding = DataBindingUtil.bind(itemView!!)!!
    }
}