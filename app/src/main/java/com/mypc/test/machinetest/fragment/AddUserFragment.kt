package com.mypc.test.machinetest.fragment


import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.text.InputFilter
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.mypc.test.machinetest.R
import com.mypc.test.machinetest.activity.MainActivity
import com.mypc.test.machinetest.model.DefineInput
import com.mypc.test.machinetest.model.User
import kotlinx.android.synthetic.main.fragment_add_user.*
import java.util.*


@SuppressLint("ValidFragment")
class AddUserFragment : Fragment(), AdapterView.OnItemSelectedListener {
    private lateinit var itemSelected: String
    override fun onNothingSelected(parent: AdapterView<*>?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        itemSelected = parent?.getItemAtPosition(position).toString()
    }

    companion object {
        val TAG = AddUserFragment::class.simpleName

        fun newInstance(bundle: Bundle) = AddUserFragment().apply {
            this.arguments = bundle
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_add_user, container, false)
    }

    private lateinit var minAge: String
    private lateinit var maxAge: String

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as AppCompatActivity).supportActionBar?.title = getString(R.string.add_user_title)
        val inputArray: ArrayList<DefineInput>? = arguments?.getParcelableArrayList<DefineInput>(getString(R.string.input_json_key))
        if (inputArray != null && !inputArray.isEmpty()) {
            for (i in 0 until inputArray.size) {
                when (inputArray[i].fieldName) {
                    getString(R.string.name_key) -> {
                        if (inputArray[i].required)
                            til_name_holder.visibility = View.VISIBLE
                    }
                    getString(R.string.age_key) -> {
                        if (inputArray[i].required) {
                            til_age_holder.visibility = View.VISIBLE
                            minAge = inputArray[i].min
                            maxAge = inputArray[i].max
                        }
                    }
                    getString(R.string.sex_key) -> {
                        if (inputArray[i].required)
                            sp_gender.visibility = View.VISIBLE
                        val dataAdapter = ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, inputArray[i].options)
                        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                        sp_gender.adapter = dataAdapter
                        sp_gender.onItemSelectedListener = this
                    }
                    getString(R.string.address_key) -> {
                        if (inputArray[i].required) {
                            til_address_holder.visibility = View.VISIBLE
                            if (inputArray[i].multiLine) {
                                til_address_holder.counterMaxLength = 200
                                val filterArray = arrayOfNulls<InputFilter>(1)
                                filterArray[0] = InputFilter.LengthFilter(200)
                                et_address.filters = filterArray
                                et_address.maxLines = 4
                            } else {
                                til_address_holder.counterMaxLength = 20
                                val filterArray = arrayOfNulls<InputFilter>(1)
                                filterArray[0] = InputFilter.LengthFilter(20)
                                et_address.filters = filterArray
                                et_address.inputType = InputType.TYPE_CLASS_TEXT
                                et_address.setLines(1)
                            }
                        }
                    }
                }
            }
        }
        btn_done.setOnClickListener {
            val user = User("", "", "", "")
            if (til_name_holder.visibility == View.VISIBLE) {
                if (et_name.text.isEmpty()) {
                    et_name.error = getString(R.string.name_empty_message)
                    et_name.requestFocus()
                    return@setOnClickListener
                } else
                    user.name = et_name.text.toString().trim()
                if (til_age_holder.visibility == View.VISIBLE) {
                    if (et_age.text.isEmpty()) {
                        et_age.error = getString(R.string.age_empty_message)
                        et_age.requestFocus()
                        return@setOnClickListener
                    } else
                        if (!minAge.isEmpty() && !maxAge.isEmpty()) {
                            if ((Integer.parseInt(et_age.text.toString()) < Integer.parseInt(minAge) || Integer.parseInt(et_age.text.toString()) > Integer.parseInt(maxAge))) {
                                et_age.setText("")
                                et_age.requestFocus()
                                et_age.error = "Age should be in between ".plus(minAge).plus(" & ").plus(maxAge)
                                return@setOnClickListener
                            } else
                                user.age = et_age.text.toString().trim()
                        }else{
                            user.age = et_age.text.toString().trim()
                        }
                }
                if (til_address_holder.visibility == View.VISIBLE) {
                    if (et_address.text.isEmpty()) {
                        et_address.error = getString(R.string.address_empty_message)
                        return@setOnClickListener
                    } else
                        user.address = et_address.text.toString().trim()
                }
                if (sp_gender.visibility == View.VISIBLE) {
                    user.address = itemSelected
                }
            }
            (activity as MainActivity).updateUser(user)
            activity?.onBackPressed()
        }
    }
}
