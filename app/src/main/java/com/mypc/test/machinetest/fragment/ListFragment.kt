package com.mypc.test.machinetest.fragment


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.mypc.test.machinetest.R
import com.mypc.test.machinetest.activity.MainActivity
import com.mypc.test.machinetest.adapter.UserAdapter
import com.mypc.test.machinetest.interfaces.UserClickListener
import com.mypc.test.machinetest.utils.InputDialogFragment
import kotlinx.android.synthetic.main.fragment_list.*


@Suppress("NO_REFLECTION_IN_CLASS_PATH")
class ListFragment : Fragment(), UserClickListener {
    override fun onItemClickListener(position: Int) {
        Toast.makeText(context, (activity as MainActivity).listViewModel.userList[position].toString(), Toast.LENGTH_LONG).show()
    }

    companion object {
        val TAG = ListFragment::class.simpleName

        fun newInstance(bundle: Bundle) = ListFragment().apply {
            this.arguments = bundle
        }
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_list, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as AppCompatActivity).supportActionBar?.title = getString(R.string.uses_title)
        setUpRecyclerView()
        fab_add.setOnClickListener {
            val ft = fragmentManager!!.beginTransaction()
            val prev = fragmentManager!!.findFragmentByTag("dialog")
            if (prev != null)
                ft.remove(prev)
            ft.addToBackStack(null)
            val dialogFragment = InputDialogFragment()
            //  dialogFragment.setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Light_NoTitleBar);
            dialogFragment.show(ft, "dialog")
        }

    }

    override fun onResume() {
        super.onResume()
        if ((activity as MainActivity).listViewModel.userList.isEmpty()) {
            toggleEmptyViews(true)
        } else {
            toggleEmptyViews(false)
            rv_users.adapter.notifyDataSetChanged()
        }
    }


    private fun setUpRecyclerView() {
        rv_users.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        rv_users.adapter = UserAdapter(userList = (activity as MainActivity).listViewModel.userList, clickListener = this)
    }

    private fun toggleEmptyViews(noData: Boolean) {
        if (noData) {
            tv_no_record.visibility = View.VISIBLE
            rv_users.visibility = View.GONE
        } else {
            tv_no_record.visibility = View.GONE
            rv_users.visibility = View.VISIBLE
        }
    }
}
