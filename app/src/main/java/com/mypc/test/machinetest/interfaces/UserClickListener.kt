package com.mypc.test.machinetest.interfaces

interface UserClickListener {

    fun onItemClickListener(position: Int)
}