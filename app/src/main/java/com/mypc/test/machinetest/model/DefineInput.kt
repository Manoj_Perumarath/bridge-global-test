package com.mypc.test.machinetest.model

import android.os.Parcel
import android.os.Parcelable

data class DefineInput(
        val fieldName: String,
        val type: String,
        var required: Boolean = false,
        val min: String,
        val max: String,
        val multiLine: Boolean,
        var options: List<String>? = null
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readByte() != 0.toByte(),
            parcel.readString(),
            parcel.readString(),
            parcel.readByte() != 0.toByte(),
            parcel.createStringArrayList()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(fieldName)
        parcel.writeString(type)
        parcel.writeByte(if (required) 1 else 0)
        parcel.writeString(min)
        parcel.writeString(max)
        parcel.writeByte(if (multiLine) 1 else 0)
        parcel.writeStringList(options)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<DefineInput> {
        override fun createFromParcel(parcel: Parcel): DefineInput {
            return DefineInput(parcel)
        }

        override fun newArray(size: Int): Array<DefineInput?> {
            return arrayOfNulls(size)
        }
    }
}