package com.mypc.test.machinetest.model

data class User(
        var name: String,
        var age: String,
        var gender: String,
        var address: String
)