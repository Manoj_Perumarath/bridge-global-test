package com.mypc.test.machinetest.utils

import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mypc.test.machinetest.R
import com.mypc.test.machinetest.activity.MainActivity
import com.mypc.test.machinetest.fragment.AddUserFragment
import com.mypc.test.machinetest.model.DefineInput
import kotlinx.android.synthetic.main.dialog_input_check.*
import java.util.*

class InputDialogFragment : DialogFragment() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return View.inflate(context, R.layout.dialog_input_check, null)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btn_ok.setOnClickListener {
            dialog.dismiss()
            val defineInput = ArrayList<DefineInput>()
            if (cb_name.isChecked)
                defineInput.add(DefineInput(getString(R.string.name_key), getString(R.string.text_key), true, "", "", false, null))

            if (cb_sex.isChecked)
                defineInput.add(DefineInput(getString(R.string.sex_key), getString(R.string.text_key), true, "", "", false, Arrays.asList(getString(R.string.male_key),getString(R.string.female_key), getString(R.string.other_key))))

            if (cb_age.isChecked)
                defineInput.add(DefineInput(getString(R.string.age_key), getString(R.string.number_key), true, et_min_age.text.toString().trim(), et_max_age.text.toString().trim(), false, null))

            if (cb_address_required.isChecked) {
                if (cb_multiline.isChecked)
                    defineInput.add(DefineInput(getString(R.string.address_key), getString(R.string.text_key), true, "", "", true, null))
                else
                    defineInput.add(DefineInput(getString(R.string.address_key), getString(R.string.text_key), true, "", "", false, null))
            }
            val bundle = Bundle()
            bundle.putParcelableArrayList(getString(R.string.input_json_key), defineInput)

           (activity as (MainActivity)).openAddUserFragment(bundle)
        }
        btn_cancel.setOnClickListener { dialog.dismiss() }
    }

    override fun onResume() {
        super.onResume()
        dialog.window.setLayout(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
    }
}